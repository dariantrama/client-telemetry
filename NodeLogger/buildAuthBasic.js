exports.buildAuthBasic = function (id, secret) {
    return 'Basic ' + new Buffer(id + ':' + secret).toString('base64');
};
