const error = (res, errTopMsg, httpCode, errMsg, appName) => {
    return {errorTopMsg: errTopMsg, httpCode: httpCode, errorDetails: errMsg, appName: appName }
}

module.exports = error;