const express = require('express'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    logger = require('./routes/api/logger'),
    config = require("./config"), 
    cors = require('cors');

const app = express();
app.use(cors({ credentials: true }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));

app.use(bodyParser.json({ limit: '10mb', extended: true }));
app.use(cookieParser());

app.use('/images', express.static(config.imageStorePath));

app.use("/api/logger", logger);

app.get("/", (req, res) => res.send("Hello"));

const port = process.env.port || 5000;
app.listen(port, () => console.log(`Server started on port ${port}`));