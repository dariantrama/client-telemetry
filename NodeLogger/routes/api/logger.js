const
    express = require("express"),
    router = express.Router(),
    fs = require('fs'),
    uuidv1 = require('uuid/v1'),
    grayloger = require('../../GrayLog'),
    config = require("../../config"),
    validateAccess = require("../../jwtAuth");

function GetAuthCookie(req) {
    return (
        (req.cookies && typeof req.cookies.jwtAuth !== "undefined")
            ? req.cookies.jwtAuth
            : "");
}

router.post("/log", (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
    res.locals.jwtAuth = GetAuthCookie(req);
    return next();
}, (req, res, next) => validateAccess.jwtAuthRequest(req, res, next)
    , (req, res) => {
        let retObj = [];
        const obj = JSON.parse((req.body).Errors);

        for (var i = 0; i < obj.length; i++) {
            try {
                grayloger(res, obj[i].ErrorDesc, 200, "", obj[i]);
                retObj.push({ ...obj[i] });
            }
            catch (ex) { }
        }
        console.log(retObj);
        return res.status(200).json({ saved: "Log Saved Successfully.", data: retObj });
    });


router.post("/uploadimage", (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
    console.log(__dirname);
    res.locals.jwtAuth = GetAuthCookie(req);
    return next();
}, (req, res, next) => validateAccess.jwtAuthRequest(req, res, next)
    , (req, res) => {

        console.log(__dirname);
        if (req.body.Image != null) {
            let uid = uuidv1().replace(/[-]/g, "");
            let fileName = uid + ".jpg";
            let base64Data = req.body.Image.replace(/^data:image\/jpeg;base64,/, "");
            fs.writeFile(config.imageStorePath + fileName, base64Data, 'base64', function (err) {
                if (err) {
                    console.log(err);
                    return res.status(500).json(err);
                }
                else {
                    var response = {
                        message: 'Image uploaded successfully',
                        Url: `${config.imageStoreURL}/${fileName}`
                    };
                    console.log(response);
                    return res.status(200).json(response);
                }
            });
        }
    }
);

router.get("/signup", (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
    console.log(SomeServerValriable);
});

router.post("/getToken", (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
    validateAccess.getJWTToken(req, res, next);
}, (req, res) => res.status(200).json(res.locals.authData));

module.exports = router;