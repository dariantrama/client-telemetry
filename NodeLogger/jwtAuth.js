const config = require('./config'),
    buildHdrs = require("./buildAuthBasic"),
    rp = require("request-promise"),
    errors = require("./errors"),
    appName = "JSLogger";

//Verify jwt
const jwtAuthRequest = function (req, res, next) {

    if (!res.locals.jwtAuth)
        //return errors(res, "Unauthorized - Cookie not found", 401, null);
        return res.status(401).json(errors(res, "Unauthorized - Cookie not found", 401, null, appName));

    let options = {
        uri: config.authBaseUrl + "/jwt/" + res.locals.jwtAuth,
        method: 'GET',
        headers: {
            'Authorization': buildHdrs.buildAuthBasic(config.authClientInfo.clientId, config.authClientInfo.clientSecret)
        },
        resolveWithFullResponse: true
    };

    rp(options)
        .then(function (response) {
            /**
             * 200 status means current jwtAuth was still good;
             */
            if (response.statusCode == "200") {
                return next();
            }
            else {
                return res.status(401).json(errors(res, "Unauthorized", 401, "JSLogger-Error validating JWT cookie", appName));
            }
        })
        .catch(function (err) {
            return res.status(500).json(err);
        });
};

const getJWTToken = function (req, res, next) {

    let options = {
        uri: config.authBaseUrl + "/oauth/token",
        method: 'POST',
        headers: {
            'Authorization': buildHdrs.buildAuthBasic(config.authClientInfo.clientId, config.authClientInfo.clientSecret),
            "Content-Type": "application/json"
        },
        resolveWithFullResponse: true,
        body: {
            username: "",
            password: "",
            grant_type: "password",
            type: "userpass"
        },
        json: true
    };

    rp(options)
        .then(function (response) {
            if (response.statusCode == "200") {
                let options = {
                    uri: config.authBaseUrl + "/jwt",
                    method: 'POST',
                    headers: {
                        'Authorization': buildHdrs.buildAuthBasic(config.authClientInfo.clientId, config.authClientInfo.clientSecret),
                        "Content-Type": "application/json"
                    },
                    resolveWithFullResponse: true,
                    body: {
                        token: response.body.access_token
                    },
                    json: true
                };

                rp(options).then(response => {
                    res.locals.authData = response.body.message.jwtToken;
                    res.locals.isTokenGenerated = true;
                    return next();
                }).catch(err => {
                    return res.status(500).json(err);
                });
            }
            else {
                return res.status(500).json(errors(res, "Access Token Service Error", 500, "Could not generate access token", appName));
            }
        })
        .catch(function (err) {
            return res.status(500).json(err);
        });
};

module.exports = { jwtAuthRequest, getJWTToken };