'use strict';
var _ = require("lodash");
const
    pjson = require('./package.json'),
    os = require('os'),
    Gelf = require('gelf'),
    config = require('./config'),
    uuidV1 = require('uuid/v1');

/**
 * Log console and log file messages if needed
 * Send error message to client
 * Append unique ID to error messages and log messages for matching
 *
 * @param res
 * @param outmsg - client error message
 * @param httpCode - http status code
 * @param logmsg - log message
 */
module.exports = function (res, outmsg, httpCode, logmsg, params) {
    let gelf = new Gelf({
        graylogPort: config.GRAYLOG.PORT,
        graylogHostname: config.GRAYLOG.HOST_NAME,
        connection: config.GRAYLOG.CONNECTION,
        environment: config.GRAYLOG.ENVIRONMENT,
        maxChunkSizeWan: config.GRAYLOG.MAX_CHUNK_SIZE_WAN,
        maxChunkSizeLan: config.GRAYLOG.MAX_CHUNK_SIZE_LAN
    });

    const message = {
        "version": pjson.version,
        "host": os.hostname(),
        "short_message": outmsg,
        "timestamp": Date.now() / 1000,
        "level": 1,
        "_mid": uuidV1(),
        "_application": "Client Telemetry",
        ...params
    };

    gelf.on('error', err => 
    console.log(err)
    );

    gelf.emit('gelf.log', message);
};